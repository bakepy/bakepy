"""Unit tests."""

from typing import Any, Callable, List, Literal
from unittest.mock import patch, call

import pytest

import bakepy.utils


pytestmark = pytest.mark.extended
pytestmark = pytest.mark.utils


def lazy_decorator(func: Callable[..., Any]) -> Callable[..., Any]:
    """Decorate with nothing, you good-for-nothing decorator."""
    return func


@pytest.mark.parametrize(
    'func, called', [
        (bakepy.utils.be_quiet, False),
        (lazy_decorator, True),
    ]
)
def test_be_quiet(func: Callable[..., Any], called: bool) -> None:
    """Test the be_quiet decorator."""
    @func
    def verbose_function() -> None:
        print('printing something')

    with patch('sys.__stdout__') as fake_print:
        verbose_function()
        if called:
            fake_print.assert_called
        else:
            fake_print.assert_not_called


@pytest.mark.parametrize(
    'func, caught', [
        (bakepy.utils.ignore_errors, True),
        (lazy_decorator, False),
    ]
)
def test_ignore_errors(func: Callable[..., Any], caught: bool) -> None:
    """Test the ignore_errors decorator."""
    @func
    def error_causing_function() -> None:
        raise Exception

    try:
        error_causing_function()
    except Exception:  # pylint: disable=broad-except
        error_caught = False
    else:
        error_caught = True

    assert caught == error_caught


@pytest.mark.parametrize('safe', [True, False])
@pytest.mark.parametrize(
    'objs, obj', [
        (['file.1', 'file.2', 'file.3'], 'files'),
        (['dir1', 'dir2', 'dir3'], 'directories'),
    ]
)
def test_cleanup(
    objs: List[str],
    safe: bool,
    obj: Literal['files', 'directories'],
) -> None:
    """Test internals of clean_up function."""
    with patch('builtins.input', return_value='y'):
        with patch('os.remove') as fake_remove:
            with patch('shutil.rmtree') as fake_rmtree:
                bakepy.utils.rm_objects(
                    objs, safe, obj)
                if obj == 'files':
                    expected_calls = [
                        call(elem) for elem in objs
                    ]
                    fake_remove.assert_has_calls(expected_calls)
                    assert fake_rmtree.assert_not_called
                elif obj == 'directories':
                    expected_calls = [
                        call(elem, ignore_errors=True) for elem in objs
                    ]
                    fake_rmtree.assert_has_calls(expected_calls)
                    assert fake_remove.assert_not_called
