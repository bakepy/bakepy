"""LaTeX Project bakefile."""

import glob
from bakepy import Rule, RuleList, Partial, clean_up


def main() -> RuleList:
    """Return Rules."""
    mainpdf = 'main.pdf'
    ltx_flags = '--interaction=batchmode'

    include = glob.glob('include/*.tex')

    return [
        Rule('all', [mainpdf], phony=True),

        Rule('%.pdf', ['%.tex'], recipe=[
            f'pdflatex {ltx_flags} $<'
        ], phony=False),

        Rule(mainpdf, ['main.tex', *include], phony=False),

        Rule('clean', recipe=[
                Partial(clean_up, '.aux', '.out', '.log', safe=False),
            ], phony=True)
    ]
