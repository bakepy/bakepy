"""Utility tool collection for bakepy."""


def exception_handler(value: int, tag: str) -> None:
    """Handle target baking mishaps."""
    if value == -2:
        raise NoRuleForTargetError(tag)
    if value == 2:
        raise UnableToBakeTarget(tag)


class NoRuleForTargetError(Exception):
    """Exception caused when a target was requested without a rule."""

    def __init__(self, tag: str):
        """Initialize no rule for requested target exception."""
        # super(NoRuleForTargetError, self).__init__(
        super().__init__(
            f'There is no rule for target {tag}'
        )


class UnableToBakeTarget(Exception):
    """Exception caused when a target was not baked."""

    def __init__(self, tag: str):
        """Initialize unable to bake target exception."""
        super().__init__(
            f'Unable to bake target {tag}'
        )
