
Welcome to bakepy's documentation!
==================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme_link
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
