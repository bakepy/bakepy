"""LaTeX Project bakefile."""

from bakepy import Rule, RuleList


def main() -> RuleList:
    """Return Rules."""
    mainpdf = 'main.pdf'

    return [
        Rule('all', [mainpdf], phony=True),
        Rule('%.pdf', ['%.tex'], recipe=['pdflatex $<'], phony=False)
    ]
