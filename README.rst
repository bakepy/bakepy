******************
The bakepy-project
******************

|MIT license|
|doc|
|Pipeline|
|cov|
|pylint|
|docstr cov|

.. |Pipeline| image:: https://gitlab.com/bakepy/bakepy/badges/master/pipeline.svg
   :target: https://gitlab.com/bakepy/bakepy/-/commits/master

.. |MIT license| image:: https://img.shields.io/badge/License-MIT-blue.svg
   :target: https://gitlab.com/bakepy/bakepy/-/tree/master/LICENSE

.. |doc| image:: https://gitlab.com/bakepy/bakepy/-/jobs/artifacts/master/raw/sphinx/sphinx.svg?job=pages
   :target: https://bakepy.gitlab.io/bakepy/

.. |cov| image:: https://gitlab.com/bakepy/bakepy/badges/master/coverage.svg
   :target: https://gitlab.com/bakepy/bakepy/-/commits/master

.. |pylint| image:: https://gitlab.com/bakepy/bakepy/-/jobs/artifacts/master/raw/pylint/pylint.svg?job=pylint
   :target: https://gitlab.com/bakepy/bakepy/-/jobs/artifacts/master/raw/pylint/pylint.log?job=pylint

.. |docstr cov| image:: https://gitlab.com/bakepy/bakepy/-/jobs/artifacts/master/raw/docstr_cov/docstr_cov.svg?job=docstr_cov
   :target: https://gitlab.com/bakepy/bakepy/-/jobs/artifacts/master/raw/docstr_cov/docstr_cov.log?job=docstr_cov

A pure python tool for task organization and package building.

Purpose of this package
=======================

- simplified setup of repetitive tasks within a directory/workspace

- resolving specified dependencies automatically

- easy access to perform those tasks thanks to the rules names
  (just like make)

- applicable to small and large projects or tasks and programmatically setup of rules can be done with python (and all it perks)

- portable and cross-platform
  requires only python

Installation
============

The package can be installed with `pip` from `PyPI` or after downloading a local version of this project and supplying the path to the directory in where the `setup.py` is located (projects root directory).

Installation using pip
----------------------

.. code-block:: sh

    python3 pip install bakepy

Installation of a local copy
----------------------------

Clone the repository in which ever way you prefere. For example by running

.. code-block:: sh

    git clone git@gitlab.com:bakepy/bakepy.git

Install the module using

.. code-block:: sh

    pip install ./bakepy

or

.. code-block:: sh

    python pip install ./bakepy


Test local setup
----------------------

If you have a local copy of the repository, go to one of the directories within `examples <https://gitlab.com/bakepy/bakepy/tree/master/examples/>`_ and type

.. code-block:: sh

    bake

or run the full testsuite from the repositories root directory

.. code-block:: sh

    pytest

.. warning::

    For some of those examples additional packages might be required, e.g. texlive or something like that.
    And the testsuite might require additional packages like ``pytest-cov``.


Using bakepy
============

The bake-py module provides a backend similar to GNUmake.
It requires a python bakefile in which the rules are specified.
When the command `bake` is executed a file called 'bakefile.py' is expected containing those rules unless the `-b` flag is used to provide an alternative name.

.. code-block:: sh

    bake -b <mySpecialBakefile>


Setting up Rules
----------------

The rules have to be the return value of the ``main()`` function within ``bakefile.py``.

They are required to have the following type:

- a List of ``Rule`` objects (class has to be imported from bakepy)

  .. code-block:: py

      from bakepy import Rule
      def main():
          ...
          return [
              Rule(
                  'rule1', dependencies=[...],
                  order_only_dependencies=[...],
                  recipe=[...],
                  phony=True|False
              ),
              ...
          ]

- the values in that dictionary have to be tuples with the following elements

  #. dependencies (list of strings)

     .. code-block:: py

         [ 'dep_1', 'dep_2', ...]

  #. order-only dependencies (list of strings)

     for example a directory that needs to be present

     .. code-block:: py

         [ 'oodep_1', 'oodep_2', ...]

  #. recipe (list with elements of three possible types)

     - string with a command run in subprocess

       .. code-block:: py

           'echo "something"'

     - a callable without arguments or a lambda function

       .. code-block:: py

           lambda: print('stuff')

     - a `Partial` object

       .. code-block:: py

           from bakepy import Partial
           ...
           Partial(func, *args, **kwargs)

       .. note:: the class serves a similar purpose as `partial` from `functools`.

  #. flag to declare this a `phony` rule that will not produce a target, but perform a series of tasks

     .. code-block:: py

         Rule(..., phony=True|False)

     If a target is not found after performing the recipe and `phony` is not `True`, a warning is issued or error raised (depending on the command line arguments used).


Examples are provided in the `examples` directory.

Recipe related features
-----------------------

A list of implemented features

- automatic variables in string based recipe steps (mimic GNUMake)
    and in string parameters used in a ``Partial`` class.

    ======== =========================================
    pattern  replacement
    ======== =========================================
    ``$@``   target
    ``$^``   dependencies seperated by witespace
    ``$<``   first object in the list of dependencies
    ======== =========================================

    .. note:: further automatic variable support is on the To-Do-List.

- rules can be used as a dependency

- nested rules using a special syntax in string recipe steps:

  .. code-block:: py

      Rule('target', [...], [...], [..., '$bake other_target', ...], True)

- ``be_quiet`` and ``ignore_error`` modes for recipes

  - string based steps that start with

    +------------+-------------------------------------------------------+
    | pattern    | mode                                                  |
    +============+=======================================================+
    | ``@``      | run quietly (do not print the command invoked)        |
    +------------+-------------------------------------------------------+
    | ``-``      | ignore errors (ignore return status of subprocess)    |
    +------------+-------------------------------------------------------+
    | ``@-``     | run quietly and ignore errors                         |
    | and ``-@`` |                                                       |
    +------------+-------------------------------------------------------+

    .. note:: those can not be combined with nested rules starting with ``$bake``.

  - for steps made from ``Callable`` (function without arguments/ ``lambda`` or ``Partial``):
    the decorators ``@be_quiet`` and ``@ignore_errors`` are provided through ``bakepy`` and can be applied to the function.

    - `be_quiet decorator`_
    - `ignore_errors decorator`_

    .. note::
        #. ``BaseExceptions`` are not included, which preserves for example ``KeyboardInterrupt``.
        #. since the functions themself are decorated, the effects are not restricted to the rule or recipe.

- since cleaning up files is a common task in most projects a `clean_up function`_ is provided to remove files and directories based on regex comparisons

Commandline arguments for the ``bake`` executable
-------------------------------------------------

Use ``-h`` or ``--help`` to get information about the various flags avaliable for ``bake``

.. code-block:: sh

    bake -h

the output should look somewhat like this:

.. code-block:: tex

    usage: bake [-h] [-b [filename]] [-v] [-d] [-s] [-r] [-q] [target]

    bake - the pure python GNUmake alternative

    positional arguments:
      target                object to be made/baked (rule to be executed)

    optional arguments:
      -h, --help            show this help message and exit
      -b [filename], --bakefile [filename]
                            name of the local bakefile
      -v, --verbose         increase output verbosity
      -d, --dryrun          show recipe without running it
      -s, --show-targets    show list of targets (do nothing else)
      -r, --show-rules      show list of rules (do nothing else)
      -q, --quiet           decrease output verbosity


Utilities
=========

`be_quiet` decorator
--------------------

The decorator reroutes ``sys.stdout`` during the decorated functions runtime to suppress any output and restores it afterwards (using a ``try`` ... ``finally`` statement).
Calling the function shown in the example below won't print anything to the console. `[go to definition] <bakepy.html#bakepy.be_quiet>`_

.. code-block:: py

    from bakepy import be_quiet

    @be_quiet
    def shushed_func():
        """Print that won't print."""
        print('Something that will never be printed')

`ignore_errors` decorator
-------------------------

catches all exceptions that are derived from ``Exception`` and prints but does not raise then.
Since this procedure includes a quiet broad exception, it is recommended to deal with specific exceptions that may arise within the original function itself.
`[go to definition] <bakepy.html#bakepy.ignore_errors>`__

.. code-block:: py

    from bakepy import ignore_errors

    @ignore_errors
    def ignored_failing_func(a: float, b: float):
        """Something that might raise an exception."""
        return a, b

`clean_up` function
-------------------

The ``clean_up`` function allows a pattern based folder and file deletion within a given directory.

.. code-block:: py

    from bakepy import clean_up

    Rule('clean', recipe=[Partial(clean_up, 'aux', 'out', 'log')])

The pattern recognition is regex-based (using `re.search(...)`). So a match anywhere in the final part of the path will be accepted unless the regex specifically states otherwise, e.g. ending with a `'$'` or starting with `'*'`. The `documentation of 're' <https://docs.python.org/3/library/re.html>`_ provides much more insight on how to use those patterns.

An example project is given in `examples/clean <https://gitlab.com/bakepy/bakepy/tree/master/examples/clean>`_.

Beside the patterns indicating files to be removed, the keyword arguments `skip` and `skip_recursive` allow exclusion from removal.
Patterns in `skip` have to appear in the final part of the path (the folder/file in question), whereas `skip_recursive` patterns are looked for in the full path.

.. warning::
    If directories are included in the removal process, e.g.

    .. code-block:: py

        clean_up(..., include_directories=True)

    is used, a ``skip`` or even ``skip_recursive`` match on a **child** component will **not safe the parent** component. The parent directory and its content will be removed.


Depending on the parameters passed to the function (besides the patterns, or skip patterns) different sets of objects will be removed from the directory. The following table shows the effect of running in recursive mode or not and including folders in the removal.

.. role:: gray

+-----------------------------------------+----+----+----+----+
| recursive                               |  x |  x | \- | \- |
+-----------------------------------------+----+----+----+----+
| include directories                     |  x | \- |  x | \- |
+=========================================+====+====+====+====+
| clean                                   | \- | \- | \- | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/` keep                     | \- | \- | \- | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/keep/` keep.txt            | \- | \- | \- | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/keep/` remove.txt          |  x |  x | \- | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/` remove                   |  x | \- |  x | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/remove/` remove            | \+ | \- |  o | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/remove/remove/` keep.txt   |  o | \- |  o | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/remove/remove/` remove.txt | \+ |  x |  o | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/remove/` keep.txt          |  o | \- |  o | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/remove/` remove.txt        | \+ |  x |  o | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/` keep.txt                 | \- | \- | \- | \- |
+-----------------------------------------+----+----+----+----+
| :gray:`clean/` remove.txt               |  x |  x |  x |  x |
+-----------------------------------------+----+----+----+----+

Explanation of the marks used in the table above:

+------+------------------------------------------------------+
| mark |meaning                                               |
+======+======================================================+
|  \-  |not removed                                           |
+------+------------------------------------------------------+
|   x  |removed                                               |
+------+------------------------------------------------------+
|   o  |removed only because parent directory is removed      |
+------+------------------------------------------------------+
|  \+  |removed because of parent but would be removed anyway |
+------+------------------------------------------------------+
