"""The bakepy argparse setup."""

import argparse
from bakepy.types import PREFIX


def argument_parsing() -> argparse.Namespace:
    """Return namespace with parsed arguments."""
    parser = argparse.ArgumentParser(
        description='bake - the pure python GNUmake alternative',
        )

    parser.add_argument(
        'target',
        action='store',
        type=str,
        nargs='?',
        const='all',
        default='all',
        help='object to be made/baked (rule to be executed)',
        metavar='target',
        )

    parser.add_argument(
        '-b', '--bakefile',
        action='store',
        nargs='?',
        type=str,
        const='bakefile',
        default='bakefile',
        dest='bakefile',
        help='name of the local bakefile',
        metavar='filename',
        )

    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        dest='verbose',
        help='increase output verbosity',
        )

    parser.add_argument(
        '-d', '--dryrun',
        action='store_true',
        dest='dryrun',
        default=False,
        help='show recipe without running it',
        )

    parser.add_argument(
        '-s', '--show-targets',
        action='store_true',
        dest='show_targets',
        default=False,
        help='show list of targets (do nothing else)',
        )

    parser.add_argument(
        '-r', '--show-rules',
        action='store_true',
        dest='show_rules',
        default=False,
        help='show list of rules (do nothing else)',
        )

    parser.add_argument(
        '-q', '--quiet',
        action='store_true',
        dest='quiet',
        default=False,
        help='decrease output verbosity',
        )

    parser.add_argument(
        '-w', '--warnings', '--warn',
        action='store_true',
        dest='warnings',
        default=True,
        help='convert bake errors to warnings',
        )

    parser.add_argument(
        '-f', '--force',
        action='store_true',
        dest='force',
        default=False,
        help='force re-bake even if not indicated by dependencies',
        )

    args = parser.parse_args()
    if args.quiet and args.verbose:
        args.verbose = False
        print(
            f'{PREFIX}Warning: detected "-q" and "-v"\n'
            f'{" "*len(f"{PREFIX}Warning")}will ignore "-v" (run quietly)')

    return args
