"""Tests for the included set of examples."""

import os
from typing import List

from unittest.mock import patch

import pytest

pytestmark = pytest.mark.examples


@pytest.mark.slow
@pytest.mark.parametrize(
    'example, rules', [
        ('tex', ['all']),
        ('tex_complex', ['all', 'clean']),
        ('basic', ['all']),
        ('clean', ['reset', 'clean', ])
    ],
)
def test_examples(example: str, rules: List[str]) -> None:
    """Go to directory and run bake."""
    work_dir = os.getcwd()
    os.chdir(os.path.join('examples', example))
    try:
        from bakepy.bake import main as bake_main
        for rule in rules:
            with patch('sys.argv', new=['bake', rule]):
                bake_main()
    finally:
        os.chdir(work_dir)


@pytest.mark.usefixtures('reset_bakefile')
@pytest.mark.utils
@pytest.mark.parametrize(
    'rule', [
        'clean',
        'clean_verbose',
        'clean_no_recurse',
        'clean_with_dirs',
        'clean_no_recurse_with_dirs',
        'clean_safe',
        'clean_no_recurse_safe',
        'clean_with_dirs_safe',
        'clean_no_recurse_with_dirs_safe',
        'clean_fails',
        'clean_abs_path_base',
        'clean_not_a_dir',
    ],
)
def test_clean(rule: str) -> None:
    """Go to directory and run bake."""
    cwd = os.getcwd()
    os.chdir('examples/clean')
    try:
        from bakepy.bake import main as bake_main
        with patch('sys.argv', new=['bake', 'reset']):
            bake_main()
        with patch('sys.argv', new=['bake', rule]):
            if 'safe' in rule:
                with patch('builtins.input', return_value='y'):
                    bake_main()
            else:
                bake_main()
        with patch('sys.argv', new=['bake', 'tear_down']):
            bake_main()
    finally:
        os.chdir(cwd)
