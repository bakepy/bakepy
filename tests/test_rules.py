"""Unit tests."""

import os
import shutil
import subprocess
import sys

from types import SimpleNamespace
from typing import Any, Callable, List, Optional, Tuple
from unittest.mock import patch

import pytest

from bakepy.rules import Rules, check_mode, remove_duplicates
from bakepy import Rule, Partial


pytestmark = pytest.mark.extended
pytestmark = pytest.mark.rules


def create_file(
    filename: str,
    content: str = 'dummy content'
) -> None:
    """Create a file with name filename."""
    with open(filename, 'w') as f_name:
        f_name.write(content)


def boomerang(arg: str, arg2: str = '') -> str:
    """Return what you gave."""
    return arg + arg2


def no_arg_1() -> None:
    """Do nothing."""


def no_arg_2() -> str:
    """Do nothing again."""
    return 'yay'


# various recipe steps
RCPS = SimpleNamespace(
    #
    # inconsequential commands
    #
    # working string rules
    str_v='echo str_v',
    str_q='@echo str_q',
    str_i='-echo str_i',
    str_iq='-@echo str_iq',
    str_qi='-@echo str_qi',
    #
    # non working string rules
    str_f='notecho str_i',
    str_fi='-notecho str_i',
    #
    # nested rule
    str_bk='$bake nested',
    str_ibk='echo inside nested',
    #
    # plain function
    lbd=lambda: print('x'),
    lbd2=lambda: print(1),
    lbd3=lambda: boomerang('x'),
    lbd4=lambda: boomerang('y'),
    #
    # no arg func
    no_arg=no_arg_1,
    no_arg2=no_arg_2,
    #
    # arg function
    argfunc_num=Partial(print, 1),
    autoargfunc=Partial(print, '$@'),
    autoargfunc_exp=Partial(print, 'test4.out'),
    #
    # arg func that returns something
    fnc_x=Partial(boomerang, '1',),
    fnc_x2=Partial(boomerang, '1', '2'),
    #
    # commands with consequences
    #
    str_tar='echo "tar content" > tar.txt',
    fnc_tar=Partial(create_file, 'fnc_tar.txt', 'tar content'),
    fnc_tar_rm=Partial(os.remove, 'fnc_tar.txt'),
    fnc_dir=Partial(os.mkdir, 'fnc_dir'),
    #
    fnc_dep=Partial(shutil.copy, 'tar.txt', 'out.txt'),
    #
    fnc_chdir=Partial(os.chdir, 'fnc_dir'),
    #
    str_pat='echo $< > $@',
    str_pat_exp='echo test.inp > test.out',
    str_pat2='echo "$^" > $@',
    str_pat2_exp='echo "test2.inp test2b.inp" > test2.out',
    str_inp='echo "arbitrary inp" > $@',
    str_pat3_exp='echo  > test3.out',
)

# Set of rules that includes every type of recipe

RULES = Rules(
    {
        # phony, no dependencies
        'pho_v': Rule('pho_v', [], [], [RCPS.str_v], phony=True),
        'pho_q': Rule('pho_q', [], [], [RCPS.str_q], phony=True),
        'pho_i': Rule('pho_i', [], [], [RCPS.str_i], phony=True),
        'pho_iq': Rule('pho_iq', [], [], [RCPS.str_iq], phony=True),
        'pho_qi': Rule('pho_qi', [], [], [RCPS.str_qi], phony=True),
        'pho_f': Rule('pho_f', [], [], [RCPS.str_f], phony=True),
        'pho_fi': Rule('pho_fi', [], [], [RCPS.str_fi], phony=True),
        'nest': Rule('nest', [], [], [RCPS.str_bk], phony=True),
        'nested': Rule('nested', [], [], [RCPS.str_ibk], phony=True),
        'lbd': Rule('lbd', [], [], [RCPS.lbd], phony=True),
        'lbd2': Rule('lbd', [], [], [RCPS.lbd2], phony=True),
        'argfunc': Rule('argfunc', [], [], [RCPS.argfunc_num], phony=True),
        'autoargfunc': Rule(
            'autoargfunc', [], [], [RCPS.autoargfunc], phony=True),
        'boomerang': Rule('boomerang', [], [], [RCPS.fnc_x], phony=True),
        'boomerang2': Rule('boomerang2', [], [], [RCPS.fnc_x2], phony=True),
        'lbd3': Rule('lbd3', [], [], [RCPS.lbd3], phony=True),
        'lbd4': Rule('lbd4', [], [], [RCPS.lbd4], phony=True),
        'no_arg': Rule('no_arg', [], [], [RCPS.no_arg], phony=True),
        'no_arg2': Rule('no_arg2', [], [], [RCPS.no_arg2], phony=True),
        #
        # phony, dependencies
        'dep_pho_v': Rule('dep_pho_v', ['pho_v'], [], [], phony=True),
        #
        # non phony
        'tar.txt': Rule('tar.txt', [], [], [RCPS.str_tar], phony=False),
        'fnc_tar.txt': Rule(
            'fnc_tar.txt', [], [], [RCPS.fnc_tar], phony=False),
        'fnc_dir': Rule('fnc_dir', [], [], [RCPS.fnc_dir], phony=False),
        #
        # non phony deps
        'out.txt': Rule(
            'out.txt', ['tar.txt'], [], [RCPS.fnc_dep], phony=False),
        #
        # phony deps & consequences
        'fnc_tar_rm': Rule(
            'fnc_tar_rm', ['fnc_tar.txt'], [], [RCPS.fnc_tar_rm], phony=True),
        'fnc_chdir': Rule(
            'fnc_chdir', [], ['fnc_dir'], [RCPS.fnc_chdir], phony=True),
        #
        # pattern rule
        '%.out': Rule('%.out', ['%.inp'], [], [RCPS.str_pat], phony=False),
        '%.inp': Rule('%.inp', [], [], [RCPS.str_inp], phony=False),
        'jep.out': Rule('jep.out', ['jep.inp'], [], [], phony=False),
        'jep.out_exp': Rule(
            'jep.out', ['jep.inp'], [], [RCPS.str_pat], phony=False),
        'another.out_exp': Rule(
            'another.out', ['another.inp'], [], [RCPS.str_pat], phony=False),
        #
        'a.nope': Rule('a.nope', [], [], [], phony=False),
        #
        # automatic var rule
        'test.out': Rule(
            'test.out', ['test.inp'], [], [RCPS.str_pat], phony=False),
        'test.out_exp': Rule(
            'test.out', ['test.inp'], [], [RCPS.str_pat_exp], phony=False),
        'test2.out': Rule(
            'test2.out', ['test2.inp', 'test2b.inp'], [],
            [RCPS.str_pat2], phony=False),
        'test2.out_exp': Rule(
            'test2.out', ['test2.inp', 'test2b.inp'], [],
            [RCPS.str_pat2_exp], phony=False),
        'test3.out': Rule(
            'test3.out', [], [], [RCPS.str_pat, RCPS.lbd], phony=False),
        'test3.out_exp': Rule(
            'test3.out', [], [], [RCPS.str_pat3_exp, RCPS.lbd], phony=False),
        'test4.out': Rule(
            'test4.out', recipe=[RCPS.autoargfunc, RCPS.argfunc_num],
            phony=False),
        'test4.out_exp': Rule(
            'test4.out', [], [],
            [RCPS.autoargfunc_exp, RCPS.argfunc_num], phony=False),
    }
)


@pytest.fixture(autouse=True)
def fixture_rules() -> None:
    """Reset flags in RULES."""
    RULES.set_flags(dry=None, warnings=True, quiet=False, verbose=False)


##################################################
# test 4 sorts of recipe steps
##################################################

@pytest.mark.parametrize(
    'inp, out',
    [
        (RCPS.str_iq, (True, True)),
        (RCPS.str_qi, (True, True)),
        (RCPS.str_i, (False, True)),
        (RCPS.str_q, (True, False)),
        (RCPS.str_v, (False, False)),
    ]
)
def test_check_mode(inp: str, out: Tuple[bool, bool]) -> None:
    """Test if the check for @ and - at the start of a command work."""
    assert check_mode(inp) == out


##################################################
# test of Rule and recipe comparison
##################################################

@pytest.mark.parametrize('one', (
    'pho_v', 'argfunc', 'boomerang', 'test.out', 'lbd', 'fnc_dir'
))
def test_rule_eq(one: str) -> None:
    """Test comparison function of Rule."""
    assert RULES.rules[one] == RULES.rules[one]


@pytest.mark.xfail(strict=True, raises=NotImplementedError)
def test_rule_eq_non_recipe_fail() -> None:
    """Test NotImplemented exception in comparison."""
    RULES.rules['pho_v'] == 42


@pytest.mark.xfail(strict=True, raises=NotImplementedError)
def test_partial_eq_non_partial_fail() -> None:
    """Test NotImplemented exception in comparison."""
    RCPS.argfunc_num == 42


@pytest.mark.xfail(strict=True, raises=AssertionError)
@pytest.mark.parametrize('one, two', [
    # ('lbd', 'lbd2')  # does not work
    ('pho_v', 'lbd'),  # recipe step types
    ('argfunc', 'lbd'),  # ...
    ('lbd', 'pho_v'),  # ...
    ('argfunc', 'boomerang'),  # different function
    ('autoargfunc', 'argfunc'),  # different arguments in same function
    ('boomerang', 'boomerang2'),  # different number of arguments
    ('no_arg', 'no_arg2'),  # different functions (without args)
])
def test_rule_eq_recipe_fail(one: str, two: str) -> None:
    """Test comparison function of Rule."""
    assert RULES.rules[one].eq_recipe(RULES.rules[two])


##################################################
# test of automatic variable replacement
##################################################

@pytest.mark.parametrize(
    'inp, out',
    [
        ('test.out', 'test.out_exp'),
        ('test2.out', 'test2.out_exp'),
        ('test3.out', 'test3.out_exp'),
        ('test4.out', 'test4.out_exp'),
    ]
)
def test_rule_parse_automatic_variables(inp: str, out: str) -> None:
    """Test automatic variable replacement in recipe strings."""
    result = RULES.rules[inp].parse_automatic_variables()
    assert result == RULES.rules[out]


##################################################
# test string recipe steps
##################################################

@pytest.mark.parametrize(
    'rule, quiet', (
        ('pho_v', False),
        ('pho_i', False),
        ('pho_fi', False),
        ('pho_qi', True),
        ('pho_iq', True),
        ('pho_q', True),
        pytest.param('pho_f', False, marks=pytest.mark.xfail(
            strict=True, raises=subprocess.CalledProcessError
        )),
        # failing recipe without ignore_error prefix
    )
)
def test_rule_bake_string_step(rule: str, quiet: bool) -> None:
    """Test the Rule.bake_string_step method."""
    step = RULES.rules[rule].recipe[0]
    RULES.set_flags(dry=False, warnings=False)

    assert isinstance(step, str)
    stripped = step.lstrip('-@')
    with patch(
        'subprocess.run',
        side_effect=subprocess.run
    ) as fake_subproc_run:
        with patch('builtins.print') as fake_print:
            RULES.rules[rule].bake_string_step(step)
            fake_subproc_run.assert_called_with(
                stripped, shell=True, check=False)
            if quiet:
                assert fake_print.assert_not_called
            else:
                fake_print.assert_called_with(stripped)


##################################################
# test function step execution
##################################################

@pytest.mark.parametrize('step', [
    RCPS.no_arg, RCPS.no_arg2
])
def test_rule_bake_func_step(step: Callable[..., Any]) -> None:
    """Test recipe step with a callable."""
    with patch(f'test_rules.{step.__name__}', side_effect=step) as mocked:
        Rule.bake_func_step(mocked)
        assert mocked.call_count == 1
        mocked.assert_called_once()


@pytest.mark.parametrize('step', [
    RCPS.fnc_x, RCPS.fnc_x2
])
def test_rule_bake_argfunc_step(step: Partial) -> None:
    """Test recipe step with a Partial."""
    with patch.object(
        step, 'func',
        side_effect=step.run()
    ) as mocked:
        Rule.bake_argfunc_step(step)
        mocked.assert_called_once_with(*step.args, **step.kwargs)


##################################################
# test nested bake step
##################################################

def takes_one(arg: Any) -> Any:
    """Return what you got."""
    return arg


def test_rule_bake_bake_step() -> None:
    """Test nested bake (string) step."""
    step = RULES.rules['nest'].recipe[0]
    assert isinstance(step, str)
    with patch('bakepy.rules.Rules.make_tar', side_effect=takes_one) as mocked:
        RULES.rules['nest'].bake_bake_step(step)
        mocked.assert_called_once()
        mocked.assert_called_once_with(step[5:].strip())


##################################################
# test bake rule including dry run
##################################################

@pytest.mark.parametrize('dry', [True, False])
@pytest.mark.parametrize(
    'rule, func', [
        ('pho_v', 'bake_string_step'),
        ('lbd', 'bake_func_step'),
        ('nest', 'bake_bake_step'),
        ('argfunc', 'bake_argfunc_step'),
    ]
)
def test_rule_bake(rule: str, func: str, dry: bool) -> None:
    """Test the bake method in Rule class."""
    RULES.dryrun = dry
    with patch(
        f'bakepy.rules.Rule.{func}',
        side_effect=takes_one
    ) as mocked:
        RULES.rules[rule].bake()
        if dry:
            mocked.assert_not_called()
        else:
            mocked.assert_called_once()


##################################################
# test duplicate removal in lists
##################################################

@pytest.mark.parametrize(
    'mocked', (True, False)
)
def test_remove_duplicates(mocked: bool) -> None:
    """Test if remove_duplicates retains order during removal."""
    test_list = [1, 8, 5, 1, 4, 5, 6]
    control_list = [1, 8, 5, 4, 6]

    if mocked:
        with patch.object(sys, 'version_info') as v_info:
            v_info.minor = 5
            result = remove_duplicates(test_list)
    else:
        result = remove_duplicates(test_list)

    assert result == control_list


##################################################
# test pattern rules
##################################################

@pytest.mark.parametrize('verb', [True, False])
@pytest.mark.parametrize(
    'inp, out',
    [
        ('jep.out', 'jep.out_exp'),
        ('no_match', None)
    ]
)
def test_rules_get_pattern_rule(
    inp: str,
    out: Optional[str],
    verb: bool
) -> None:
    """Test the pattern rule adaption."""
    RULES.verbose = verb
    result = RULES.get_pattern_rule(inp)
    if out is not None:
        assert result == RULES.rules[out]
    else:
        assert result is None


##################################################
# test get correct rule
##################################################

@pytest.mark.parametrize('verb', [True, False])
@pytest.mark.parametrize(
    'inp, out',
    [
        ('jep.out', 'jep.out_exp'),
        ('another.out', 'another.out_exp')
    ]
)
def test_rules_get_correct_rule(
    inp: str,
    out: Optional[str],
    verb: bool
) -> None:
    """Test the correct rule detector with pattern rules."""
    RULES.verbose = verb
    result = RULES.get_correct_rule(inp)
    if out is not None:
        assert result == RULES.rules[out]
    else:
        assert result is None


##################################################
# test dependency check
##################################################

@pytest.mark.parametrize(
    'rule', ['test.out', 'fnc_chdir']
)
def test_rules_check_dependencies(rule: str) -> None:
    """Test the dependency maker."""
    deps = RULES.rules[rule].deps + RULES.rules[rule].order_only_deps
    with patch('bakepy.rules.Rules.make_tar') as fake_make:
        RULES.check_deps(RULES.rules[rule])
        for i in deps:
            fake_make.assert_any_call(i)


##################################################
# test make_inner_tar
##################################################

@pytest.mark.parametrize(
    'rule', ['test.out', 'fnc_chdir', 'no_rule_for_you']
)
def test_rules_make_inner_tar(rule: str) -> None:
    """Test make_inner_tar function as far as possible."""
    try:
        deps: Optional[List[str]] = (
            RULES.rules[rule].deps + RULES.rules[rule].order_only_deps
        )
    except KeyError:
        deps = None
    with patch('bakepy.rules.Rule.bake') as fake_bake:
        with patch('bakepy.rules.Rules.check_deps') as fake_dep_check:
            RULES.make_tar_inner(rule)
            if deps is not None:
                fake_bake.assert_called_once()
                fake_dep_check.assert_called_once_with(RULES.rules[rule])
            else:
                fake_dep_check.assert_not_called()


##################################################
# test string representations
##################################################

@pytest.mark.parametrize('rule', [
    'test.out', 'jep.out', 'pho_v', 'nest', 'fnc_dir'
])
def test_string_representation(rule: str) -> None:
    """Test string representation."""
    print(RULES.rules[rule])
    print(repr(RULES.rules[rule]))


##################################################
# test set_flags function of Rules
##################################################

@pytest.mark.parametrize('dry', [False, True])
@pytest.mark.parametrize('warn', [False, True])
@pytest.mark.parametrize('verb', [False, True])
@pytest.mark.parametrize('quiet', [False, True])
@pytest.mark.parametrize('force', [False, True])
def test_rules_set_flags(
    dry: bool,
    warn: bool,
    verb: bool,
    quiet: bool,
    force: bool,
) -> None:
    """Test Rules flag setting."""
    RULES.set_flags(
        dry=dry,
        warnings=warn,
        quiet=quiet,
        verbose=verb,
        force=force,
    )
    exp_verb = verb if not quiet or not verb else False
    expected = (dry, warn, quiet, exp_verb)
    result = (
        RULES.dryrun, RULES.warnings,
        RULES.quiet, RULES.verbose
    )
    assert expected == result
