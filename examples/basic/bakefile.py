"""A bakefile that declares the rules of baking."""

import os
from bakepy import (
    Partial, Rule,
    be_quiet, ignore_errors,
    RuleList
)


@be_quiet  # suppress print
@ignore_errors  # without the decorator rule1 fails
def func_rule1(arg1: int, arg2: int) -> None:
    """Test for ignore_errors decorator."""
    print(arg1/arg2)


def func_rule2() -> None:
    """Just a print thingy."""
    print('I am func_rule2')


def main() -> RuleList:
    """Return the rules and the tar."""
    return [
        Rule(
            'all', ['rule1', 'rule2'], recipe=[Partial(print, 'hello')],
            phony=True),

        Rule('rule1', [], [], [Partial(func_rule1, 2, 0)], phony=True),
        Rule('rule2', [], [], [Partial(func_rule1, 2, 2)], phony=True),
        Rule('rule3', [], [], [func_rule2], phony=True),

        Rule(
            'test.tar', ['test.inp'], [], ['echo $@ needs $<'], phony=True),

        Rule('chainedrule', [], [], ['$bake rule1'], phony=True),

        Rule('parentrule', [], ['subdir'], [
            Partial(os.chdir, 'subdir'),
            '$bake subrule',
            Partial(os.chdir, '..'),
            Partial(print, os.getcwd()),
            ], phony=True),

        Rule('subrule', [], [], [
            Partial(print, os.getcwd()),
            'echo subdirfile.inp',
            ], phony=True),
    ]
