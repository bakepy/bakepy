"""Pytest configurations and global fixtures."""

import sys
from typing import Iterator

import pytest


@pytest.fixture
def reset_bakefile() -> Iterator[None]:
    """Remove loaded bakefile if present."""
    # the following section is necessary to ensure no other
    # bakefile remains loaded during tests
    try:
        del sys.modules['bakefile']
    except KeyError:
        pass
    yield
