"""Unit tests."""

import pytest

from bakepy.rules import (
    Rule, Rules,
)
from bakepy.errors import (
    NoRuleForTargetError, UnableToBakeTarget
)


pytestmark = pytest.mark.extended
pytestmark = pytest.mark.errors


def test_unabale_to_bake() -> None:
    """Test unable to bake exception."""
    tar = 'tarNotBaked'
    test_rule = Rule(tar, [], [], ['@echo doing nothing'], phony=False)
    rules = Rules({tar: test_rule})
    rules.rules[tar].rules = rules
    try:
        rules.make_tar(tar)
    except UnableToBakeTarget:
        pass
    else:
        raise AssertionError


def test_no_rule_for_tar() -> None:
    """Test unable to bake exception."""
    faketar = 'tarWithoutRule'
    tar = 'tarWithRule'
    test_rule = Rule(tar, [], [], ['@echo doing nothing'], phony=False)
    rules = Rules({tar: test_rule})
    rules.rules[tar].rules = rules
    try:
        rules.make_tar(faketar)
    except NoRuleForTargetError:
        pass
    else:
        raise AssertionError
