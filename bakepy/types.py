"""Types for bake-py."""

from typing import (
    Any, Callable, Union
)

# print statement prefix
PREFIX = '$ bake'

# recipe combined
RecipeStep = Union[str, Callable[..., Any], 'Partial']


class Partial:
    """
    Instance of a function to be run later.

    This is similar to the functools partial utility
    but it allows more insight and and comparison with
    other objects of this kind.
    """

    def __init__(
        self,
        func: Callable[..., Any],
        *args: Any,
        **kwargs: Any
    ):
        """Store arguments and the function to be run."""
        self.name = func.__name__
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def run(self) -> Any:
        """Trigger function."""
        return self.func(*self.args, **self.kwargs)

    def __eq__(self, other: Any) -> bool:
        """Compare two instances."""
        if not isinstance(other, Partial):
            raise NotImplementedError
        return (
            self.func.__name__ == other.func.__name__
            and self.args == other.args
            and self.kwargs == self.kwargs
        )

    def __repr__(self) -> str:
        """Return human readable representation of instance."""
        return(
            f'<Partial_Object: func={self.name}, '
            f'args={str(self.args)}, '
            f'keyword_args={str(self.kwargs)}>'
        )
