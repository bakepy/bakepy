"""Unit tests."""

import os

from typing import List
from unittest.mock import patch

import pytest

from bakepy.parser import argument_parsing


pytestmark = pytest.mark.extended
pytestmark = pytest.mark.parser


@pytest.mark.usefixtures('reset_bakefile')
@pytest.mark.parametrize(
    'flags',
    [
        ['-b', 'bakefile'],
        ['--bakefile', 'bakefile'],
        [],
        pytest.param(
            ['-b', 'noBakefile'], marks=pytest.mark.xfail(strict=True)),
    ]
)
def test_bakefile_argparse(flags: str) -> None:
    """Test the bakefile name flag for bake."""
    cwd = os.getcwd()
    os.chdir('examples/basic')
    try:
        from bakepy.bake import main as bake_main
        with patch('sys.argv', new=['bake', *flags]):
            bake_main()
    finally:
        os.chdir(cwd)


@pytest.mark.usefixtures('reset_bakefile')
@pytest.mark.parametrize(
    'flags',
    [
        (['-v', 'parentrule']),
    ]
)
def test_target_argparse(flags: str) -> None:
    """Test the verbosity related flags."""
    cwd = os.getcwd()
    os.chdir('examples/basic')
    try:
        from bakepy.bake import main as bake_main
        with patch('sys.argv', new=['bake', *flags]):
            bake_main()
    finally:
        os.chdir(cwd)


@pytest.mark.usefixtures('reset_bakefile')
@pytest.mark.parametrize(
    'flags',
    [
        (['-s']),
        (['-r']),
    ]
)
def test_various(flags: str) -> None:
    """Test the verbosity related flags."""
    cwd = os.getcwd()
    os.chdir('examples/basic')
    try:
        from bakepy.bake import main as bake_main
        with pytest.raises(SystemExit) as wrapped_exit:
            with patch('sys.argv', new=['bake', *flags]):
                bake_main()
        assert wrapped_exit.value.code == 0

    finally:
        os.chdir(cwd)


@pytest.mark.parametrize(
    'flags, expected',
    [
        ('-s', True),
        ('--show-targets', True),
        ('', False),
    ]
)
def test_show_targets_argparse(flags: str, expected: int) -> None:
    """Test the targets related flags."""
    with patch('sys.argv', new=['bake', flags]):
        args = argument_parsing()
    assert args.show_targets is expected


@pytest.mark.parametrize(
    'flags, expected',
    [
        ('-r', True),
        ('--show-rules', True),
        ('', False),
    ]
)
def test_show_rules_argparse(flags: str, expected: int) -> None:
    """Test the rules related flags."""
    with patch('sys.argv', new=['bake', flags]):
        args = argument_parsing()
    assert args.show_rules is expected


@pytest.mark.parametrize(
    'flags, expected',
    [
        ('-w', True),
        ('', True),
        ('--warnings', True),
        ('--warn', True),
    ]
)
def test_warnings_argparse(flags: str, expected: bool) -> None:
    """Test warning flag."""
    with patch('sys.argv', new=['bake', flags]):
        args = argument_parsing()
    assert args.warnings is expected


@pytest.mark.parametrize(
    'flags, exp_quiet, exp_verb',
    [
        (['-q'], True, False),
        (['--quiet'], True, False),
        (['-v'], False, True),
        (['--verbose'], False, True),
        (['-v', '-q'], True, False),
        (['-q', '-v'], True, False),
        ([''], False, False),
    ]
)
def test_verb_argparse(
    flags: List[str],
    exp_quiet: int,
    exp_verb: int,
) -> None:
    """Test the verbosity related flags."""
    with patch('sys.argv', new=['bake', *flags]):
        args = argument_parsing()
    assert args.verbose is exp_verb
    assert args.quiet is exp_quiet
