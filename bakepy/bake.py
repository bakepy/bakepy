#! /usr/bin/env python3
"""The bake-py main file."""

import os
import sys
import importlib
from typing import List

from .parser import argument_parsing
from .types import PREFIX
from .rules import Rules, Rule

RuleList = List[Rule]


def main() -> None:
    """Create rules and start making target."""
    # argument parsing
    args = argument_parsing()

    # import from lokal bakefile
    try:
        sys.path.insert(0, os.getcwd())
        bakefile = importlib.import_module(args.bakefile)
    except ModuleNotFoundError:
        print(
            f'\n{PREFIX}ERROR: unable to find "{args.bakefile}.py"\n\n'
            f'{" "*len(PREFIX)}the file specifying the rules is missing\n'
            f'{" "*len(PREFIX)}or you misspelled it\n'
        )
        sys.exit(1)
    finally:
        sys.path = [i for i in sys.path if i != os.getcwd()]

    pre_rules: RuleList = bakefile.main()  # type:ignore
    sec_rules = {x.tar: x for x in pre_rules}

    rules = Rules(
        sec_rules,
        dryrun=args.dryrun,
        verbose=args.verbose,
        quiet=args.quiet,
        warnings=args.warnings,
        force=args.force,
    )

    if args.show_targets:
        print(rules.available())
        sys.exit(0)

    if args.show_rules:
        print(rules)
        sys.exit(0)

    rules.make_tar(args.target)
