"""Clean example bakefile."""

import os
import shutil

from bakepy import clean_up, RuleList, Rule, Partial


def main() -> RuleList:
    """Return rules."""
    return [
        Rule('all', phony=True),

        Rule('clean', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=False,
                skip='keep',
                skip_recursive='.clean_copy',
            )
        ], phony=True),

        Rule('clean_verbose', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=False,
                skip='keep',
                skip_recursive='.clean_copy',
                verbose=True,
            )
        ], phony=True),

        Rule('clean_with_dirs', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=False,
                skip='keep',
                skip_recursive='.clean_copy',
                remove_dirs=True,
            )
        ], phony=True),

        Rule('clean_no_recurse_with_dirs', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=False,
                skip='keep',
                skip_recursive='.clean_copy',
                remove_dirs=True,
                recursive=False,
            )
        ], phony=True),

        Rule('clean_no_recurse', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=False,
                skip='keep',
                skip_recursive='.clean_copy',
                recursive=False,
            )
        ], phony=True),

        Rule('clean_safe', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                skip='keep',
                skip_recursive='.clean_copy',
            )
        ], phony=True),

        Rule('clean_with_dirs_safe', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                skip='keep',
                skip_recursive='.clean_copy',
                remove_dirs=True,
            )
        ], phony=True),

        Rule('clean_no_recurse_with_dirs_safe', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                skip='keep',
                skip_recursive='.clean_copy',
                remove_dirs=True,
                recursive=False,
            )
        ], phony=True),

        Rule('clean_no_recurse_safe', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                skip='keep',
                skip_recursive='.clean_copy',
                recursive=False,
            )
        ], phony=True),

        Rule('clean_fails', recipe=[
            Partial(
                clean_up,
                '.',
                safe=True,
                skip='keep',
                skip_recursive='.clean_copy',
                recursive=False,
            )
        ], phony=True),

        Rule('clean_abs_path_base', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                basedir=(os.path.abspath('../tex')),
                skip='keep',
                skip_recursive='.clean_copy',
                recursive=False,
            )
        ], phony=True),

        Rule('clean_not_a_dir', recipe=[
            Partial(
                clean_up,
                'remove',
                safe=True,
                basedir='notadir',
                skip='keep',
                skip_recursive='.clean_copy',
                recursive=False,
            )
        ], phony=True),

        Rule('reset', recipe=[reset], phony=True),
        Rule('tear_down', recipe=[tear_down], phony=True),
    ]


def reset() -> None:
    """Rebuild the original file structure."""
    for i in os.listdir('.clean_copy'):
        obj = os.path.join('.clean_copy', i)
        if os.path.isdir(obj):
            shutil.copytree(obj, i, dirs_exist_ok=True)
        else:
            shutil.copy(obj, i)


def tear_down() -> None:
    """Remove everything except .clean_copy and bakefile.py."""
    if not os.getcwd().endswith('clean'):
        return
    for obj in os.listdir('.'):
        if (
            '.clean_copy' in obj
            or 'bakefile.py' in obj
        ):
            continue
        if os.path.isdir(obj):
            shutil.rmtree(obj, ignore_errors=True)
        else:
            os.remove(obj)
