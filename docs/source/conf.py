"""Sphinx Configuration."""

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from typing import Any, Dict, Optional
# import sphinx_rtd_theme

sys.path.insert(
    0, os.path.abspath(
        os.path.join('..', '..', 'src')
    )
)


# -- Project information -----------------------------------------------------

project = 'bakepy'
copyright = '2021, Engel'
author = 'Engel'

# The full version, including alpha/beta/rc tags
release = '0.1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.autodoc.typehints',
    'sphinx_rtd_theme',
    'sphinx.ext.linkcode',
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
# exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = (
    # 'alabaster'
    'sphinx_rtd_theme'
    # 'groundwork'
)

html_css_files = [
    'custom.css'
]

# html_theme_options = {
# }


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- AutoAPI options ---------------------------------------------------------

def linkcode_resolve(domain: str, info: Dict[str, Any]) -> Optional[str]:
    """Provide gitlab links."""
    if domain != 'py':
        return None
    if not info['module']:
        return None
    if info['module'] == 'bakepy':
        info['module'] += '.__init__'
    filename = info['module'].replace('.', '/')
    return "https://gitlab.com/bakepy/bakepy/-/tree/master/%s.py" % filename
