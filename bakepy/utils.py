"""Provide decorators to be used in bakefiles."""

import os
import sys
import re
import pathlib
import shutil
from typing import Any, Callable, List, Literal, Optional, Tuple, Union
import io
import functools

_UNSAFE_PATTERNS = [r'*', r'.', r'*.', r'.*', '..', '.', '*', '.*', '']


def rm_objects(
    objects: List[str],
    safe: bool,
    obj: Literal['files', 'directories'],
) -> None:
    """
    Remove files or directories given in a list.

    Parameters
    ----------
    objects:
        list of paths which will be removed

    safe:
        if True, a command prompt will provide a list of the
        objects to be removed and request confirmation

    obj:
        either 'files' or 'directories' to indicate which kind of
        objects are provided so the correct function is invoked
        to remove them
    """
    if safe and len(objects) > 0:
        print(f'\nThe following {obj} will be removed:')
        print(', '.join(objects))
        inp = input('\nConfirm (y): ')
    else:
        inp = 'y'

    if inp == 'y':
        if obj == 'files':
            for a_obj in objects:
                os.remove(a_obj)

        else:
            for a_obj in objects:
                shutil.rmtree(a_obj, ignore_errors=True)


def _rm_files(
    files: List[str],
    safe: bool,
) -> None:
    """Call remove objects for files."""
    rm_objects(files, safe, 'files')


def _rm_dirs(
    dirs: List[str],
    safe: bool,
) -> None:
    """Filter directories for redundancies and call removal."""
    # incase of dirs being sub-dirs of removed dirs:
    dirs = [
        i for i in dirs
        if not any(
            i.startswith(f'{j}')
            for j in dirs
            if i != j
        )
    ]
    rm_objects(dirs, safe, 'directories')


def _clean_up_input_check(
    basedir: str,
    patterns: Tuple[str, ...],
) -> Optional[str]:
    """
    Check clean_up input parameters for validity.

    If the basedir does not exist or is not part of the current
    working directories subtree a warning is issued and the
    clean_up will be aborted.

    If the patterns include a pattern considered 'dangerous'
    a warning is printed and clean_up is aborted.
    """
    base = os.path.abspath(basedir)
    if not os.path.isdir(base):
        print(
            '\n Cleanup failed: invalid path specification\n'
            f'>>> "{basedir}"\n'
        )
        return None

    common = os.path.relpath(base)
    if common.startswith('..'):
        print(
            '\nCleanup failed: path specified outside working directory\n'
            f'>>> "{basedir}"\n'
        )
        return None

    bad_pattern = {
        i: pat for i, pat in enumerate(patterns)
        if pat in _UNSAFE_PATTERNS
    }
    if len(bad_pattern) > 0:
        print(
            '\nCleanup failed: unsafe pattern(s) detected. \n>>>'
            + '\n>>>'.join(
                [f' [{key}] "{val}"' for key, val in bad_pattern.items()])
            + '\n'
        )
        return None
    return base


def _select_objects(
    objects: List[str],
    patterns: Tuple[str, ...],
    skip: Optional[List[str]],
    skip_recursive: Optional[List[str]]
) -> List[str]:
    """
    Return a selection of path objects based on pattern matches.

    Parameters
    ----------
    objects: [str]
        List of objects from which a selection is returned

    patterns: (str,)
        Tuple of patterns that lead to an object being selected

    skip: [str]
        List of patterns matched with the final part of path
        that exclude it from selection (despite match in `patterns`)

    skip_recursive: [str]
        List of patterns that will exclude the path from selection
        if ANY part of the path matches

    """
    selected = [
        a_obj for a_obj in objects
        if any(
            re.search(pat, pathlib.PurePath(a_obj).name)
            for pat in patterns)
    ]
    if skip is not None:
        selected = [
            a_obj for a_obj in selected
            if not any(
                    re.search(pat, pathlib.PurePath(a_obj).name)
                    for pat in skip
            )
        ]
    if skip_recursive is not None:
        selected = [
            a_obj for a_obj in selected
            if not any(
                re.search(pat, a_obj)
                for pat in skip_recursive
            )
        ]
    return selected


def clean_up(
    *patterns: str,
    skip: Union[List[str], str, None] = None,
    skip_recursive: Union[List[str], str, None] = None,
    basedir: str = '.',
    recursive: bool = True,
    remove_dirs: bool = False,
    ask_again: bool = False,
    safe: bool = False,
    verbose: bool = False,
    # **kwargs: Union[bool, str, List[str]]
) -> None:
    """Regex pattern based file/directory removal tool.

    Parameters
    ----------
    patterns: str, ...
        one or more strings with regex patterns that mark a file for removal
        (use raw strings if required)

    skip: str, [str], optional
        single string or list of strings with patterns that safe the file
        from removal if one of them appears within the final part of the path

    skip_recursive: str, [str], optional
        single string or list of strings with patterns that may appear anywhere
        in the path of the given file saving it from removal

    basedir: str, optional
        root for file and directory object search (default=`.`)

    recursive: bool, optional
        `True`: include subdirectories of basedir (default);
        `False`: restrict to given basedir

    remove_dirs: bool, optional
        `False`: exclude directories from removal (default);
        `True`: include directories in removal

    ask_again: bool, optional
        `False`: do not request confirmation, just remove objects
        `True`: request confirmation before removing anything

    """
    if isinstance(skip, str):
        skip = [skip]

    if isinstance(skip_recursive, str):
        skip_recursive = [skip_recursive]

    safe = ask_again or safe

    if verbose:
        print(
            f'patterns:       {str(patterns)}\n'
            f'skip:           {str(skip)}\n'
            f'skip_recursive: {str(skip_recursive)}\n'
            f'recursive:      {str(recursive)}\n'
            f'basedir:        {str(basedir)}\n'
            f'safe:           {str(safe)}\n'
            f'remove_dirs:    {str(remove_dirs)}\n'
        )

    base = _clean_up_input_check(basedir, patterns)
    try:
        assert isinstance(base, str)
    except AssertionError:
        return

    if recursive:
        if remove_dirs:
            dirs = [x[0] for x in os.walk(base)]
            dirs = [os.path.relpath(x, os.path.abspath(base)) for x in dirs]
            rm_dirs = _select_objects(
                dirs, patterns, skip, skip_recursive)
            _rm_dirs(rm_dirs, safe)

        files = [os.path.join(x[0], y) for x in os.walk(base) for y in x[2]]
        files = [os.path.relpath(x, os.path.abspath(base)) for x in files]

        rm_files = _select_objects(files, patterns, skip, skip_recursive)
        _rm_files(rm_files, safe)
    else:
        if remove_dirs:
            dirs = [i for i in os.listdir(base) if os.path.isdir(i)]
            # if remove_dirs:
            rm_dirs = _select_objects(dirs, patterns, skip, skip_recursive)
            _rm_dirs(rm_dirs, safe)

        files = [i for i in os.listdir(base) if os.path.isfile(i)]
        rm_files = _select_objects(files, patterns, skip, skip_recursive)
        _rm_files(rm_files, safe)


def ignore_errors(func: Callable[..., Any]) -> Callable[..., Any]:
    """Catch and log every Exception so a recipe will be continued."""
    @functools.wraps(func)
    def wrapper_ignore_errors(*args: Any) -> Union[str, None]:
        """ignore_errors decorator to catch exceptions."""
        try:
            func(*args)
        except Exception as exc:  # pylint: disable=broad-except
            return str(exc)
        return None

    return wrapper_ignore_errors


def be_quiet(func: Callable[..., Any]) -> Callable[..., Any]:
    """Catch and log every Exception so a recipe will be continued."""
    @functools.wraps(func)
    def wrapper_be_quiet(*args: Any) -> None:
        """be_quiet decorator to suppress print statements."""
        save = sys.stdout
        sys.stdout = io.StringIO()
        try:
            func(*args)
        finally:
            sys.stdout = save

    return wrapper_be_quiet
