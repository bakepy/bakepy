"""New tests."""


import os
import shutil
import pathlib

from typing import Iterator

import pytest

from bakepy.types import Partial
from bakepy.errors import (
    UnableToBakeTarget,
    NoRuleForTargetError
)
from bakepy.rules import (
    Rule, Rules,
)


pytestmark = [pytest.mark.states, pytest.mark.extended]


def create_file(filename: str, content: str = 'dummy content') -> None:
    """Create a file with name filename."""
    with open(filename, 'w') as f_name:
        f_name.write(content)


def set_up(location: str) -> None:
    """Create directory with certain files to test overall."""
    if os.path.exists(location):
        shutil.rmtree(location, ignore_errors=True)

    shutil.copytree('./tests/.testdir', location)
    pathlib.Path(location + '/test.inp').touch()


def set_rules() -> Rules:
    """Set up the rules for the tests."""
    return Rules(
        {
            '%.out': Rule(
                '%.out', ['%.inp'], recipe=['echo $< > $@'], phony=False),
            'test2.out': Rule(
                'test2.out', ['test2.inp', 'test2b.inp'], phony=False),
            'subdir': Rule(
                'subdir', [], [], [Partial(os.mkdir, '$@')], phony=False),
            'subdir/.inp': Rule(
                'subdir/%.inp', [], [],
                [Partial(create_file, '$@')], phony=False),
            'broken.inp': Rule(
                'broken.inp', recipe=[Partial(print, 'no joy',)], phony=False),
            'all': Rule('all', [], [], ['echo yay'], phony=True),
        },
        verbose=True,
    )


@pytest.fixture(
    name='rules',
    scope='function',
)
def fixture_rules(dry: bool, warnings: bool) -> Iterator[Rules]:
    """Make rules and setup available."""
    where = 'temp_test_dir'
    home = os.getcwd()
    location = os.path.join(home, where)
    set_up(location)
    rules = set_rules()
    os.chdir(location)
    yield rules.set_flags(dry=dry, warnings=warnings)
    os.chdir(home)
    shutil.rmtree(location)


@pytest.mark.parametrize('warnings', [True, False])
@pytest.mark.parametrize('dry', [True, False])
def test_nothing_to_be_done(rules: Rules) -> None:
    """Test a rule were nothing is to be done."""
    for i in ['test2.out']:
        status = rules.make_tar(i)
        assert status == (0, i)


@pytest.mark.parametrize('warnings', [True, False])
@pytest.mark.parametrize('dry, istat', [(True, 10), (False, 1)])
def test_updated(rules: Rules, istat: int) -> None:
    """Test a rule where the target is updated."""
    os.system('ls -l')
    for i in os.listdir():
        print(os.stat(i).st_mtime_ns)
    for i in ['test.out']:
        status = rules.make_tar(i)
        assert status == (istat, i)


@pytest.mark.parametrize('dry, warnings', [
    (True, True),
    (False, True),
    (True, False),
    pytest.param(False, False, marks=pytest.mark.xfail(
        strict=True, raises=NoRuleForTargetError)),
])
def test_no_rule(rules: Rules) -> None:
    """Test a case where no rule applies."""
    for i in ['test3.inp']:
        status = rules.make_tar(i)
        assert status == (-2, i)


@pytest.mark.parametrize('dry, warnings, istat', [
    (True, True, 10),
    (False, True, 2),
    (True, False, 10),
    pytest.param(False, False, 2, marks=pytest.mark.xfail(
        strict=True, raises=UnableToBakeTarget)),
])
def test_unable_to_make(rules: Rules, istat: int) -> None:
    """Test a case the target remains missing."""
    for i in ['broken.inp']:
        status = rules.make_tar(i)
        assert status == (istat, i)


@pytest.mark.parametrize('warnings', [True, False])
@pytest.mark.parametrize('dry, istat', [(True, 10), (False, -1)])
def test_phony(rules: Rules, istat: int) -> None:
    """Test a simple phony rule."""
    for i in ['all']:
        status = rules.make_tar(i)
        assert status == (istat, i)
