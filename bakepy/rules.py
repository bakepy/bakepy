"""Provide the Rule class."""
from __future__ import annotations
import os
import re
import subprocess
import sys

from collections import OrderedDict
from typing import Any, Callable, List, Optional, Dict, Tuple, Union

from .types import (
    PREFIX, Partial, RecipeStep
)
from .errors import exception_handler

# Rule dictionary
RuleDictT = Dict[str, 'Rule']


RecipeStepFlags = (
    (re.compile(r'^[@-]{2}'), (True, True)),
    (re.compile(r'^[-]'), (False, True)),
    (re.compile(r'^[@]'), (True, False)),
)


def remove_duplicates(items: List[Any]) -> List[Any]:
    """Return list without duplicates but retained order."""
    if sys.version_info.minor < 7:
        # versions < 3.7 do not necessarily retain insertion order in dicts
        # so the collections OrderedDict is used instead
        return list(OrderedDict.fromkeys(items))

    return list(dict.fromkeys(items))


def check_mode(string: str) -> Tuple[bool, bool]:
    """Return flags for quiet and ignore_error signal."""
    for regex, result in RecipeStepFlags:
        if re.search(regex, string):
            return result
    return (False, False)


class Rule:
    """Store target, dependencies and recipe for a rule."""

    def __init__(
        self,
        target: str,
        dependencies: Optional[List[str]] = None,
        order_only_deps: Optional[List[str]] = None,
        recipe: Optional[List[RecipeStep]] = None,
        *,
        phony: Optional[bool] = False,
    ):
        """
        Instantiate a new rule.

        Parameters
        ----------
        target: str
            name of the target/rule to be build/performed

        dependencies: [str]
            list of objects/rules this target depends on

        order_only_deps: [str]
            list of objects/rules this target depends on, but
            only their presence is required (modification irrelevant)

        recipe: [RecipeStep]
            list of tasks to be performed in the given order
            to build this target/ perform the rule

        phony: bool
            mark as a rule where no target is build

        """
        self.tar = target
        self.deps: List[str] = (
            dependencies if dependencies is not None else [])
        self.order_only_deps: List[str] = (
            order_only_deps if order_only_deps is not None else [])
        self.recipe: List[RecipeStep] = (
            recipe if recipe is not None else [])
        self.phony = phony
        self.rules: Optional[Rules] = None

    def adapt_recipe(self, temp: Rule) -> Rule:
        """Copy recipe from temp to this rule."""
        self.recipe = temp.recipe
        return self

    def eq_recipe(self, other: Rule) -> bool:
        """Return true if recipes seem to be the same."""
        for i, step in enumerate(self.recipe):
            other_step = other.recipe[i]
            if isinstance(step, str):
                if other_step != step:
                    return False
            elif isinstance(step, Partial):
                if not isinstance(other_step, Partial):
                    return False
                if not step == other_step:
                    return False
            else:
                if not callable(other_step):
                    return False
                if step.__name__ != other_step.__name__:
                    return False
        return True

    def __eq__(self, other: Any) -> bool:
        """Compare two Rule objects, return True if equal."""
        if not isinstance(other, Rule):
            raise NotImplementedError
        return all([
            self.tar == other.tar,
            self.deps == other.deps,
            set(self.order_only_deps) == set(other.order_only_deps),
            self.eq_recipe(other),
            self.phony == other.phony,
        ])

    def parse_automatic_variables(self) -> Rule:
        """
        Replace placeholders for automatic variables in recipes.

        This is only relevant in string based steps.

        List of placeholders:

        - $@ : target
        - $< : first dependency
        - $^ : all dependencies (order only excluded)

        not implemented yet:

        - $(@D) : directory part of target
        - $(@F) : basename and suffix part of target
        """
        if len(self.deps) == 0:
            first_dep = ''
        else:
            first_dep = self.deps[0]

        replacements = [
            ('$@', self.tar),
            ('$<', first_dep),
            ('$^', " ".join(self.deps))
        ]

        processed: List[Any] = []
        for step in self.recipe:
            if not isinstance(step, str):
                if isinstance(step, Partial):
                    # this is a function with args
                    processed.append(
                        self._parse_partial_arguments(
                            step, replacements
                        )
                    )
                else:
                    processed.append(step)
            else:
                for var, repl in replacements:
                    step = step.replace(var, repl)
                processed.append(step)

        self.recipe = processed

        return self

    @classmethod
    def _parse_partial_arguments(
        cls,
        step: Partial,
        replacements: List[Tuple[str, str]],
    ) -> RecipeStep:
        """Replace automatic variables in Partial arguments."""
        processed_args: List[Any] = []
        for arg in step.args:
            if isinstance(arg, str):
                for var, repl in replacements:
                    arg = arg.replace(var, repl)
                processed_args.append(arg)
            else:
                processed_args.append(arg)
            step.args = tuple(processed_args)
        return step

    def bake_string_step(self, step: str) -> None:
        """Run a system call function given as a string."""
        assert self.rules is not None
        be_quiet, ignore_err = check_mode(step)
        if be_quiet and ignore_err:
            step = step[2:]
        elif be_quiet:
            step = step[1:]
        elif ignore_err:
            step = step[1:]
            if not self.rules.quiet:
                print(step)
        else:
            if not self.rules.quiet:
                print(step)
        stat = subprocess.run(step, shell=True, check=False).returncode
        if not (
            stat == 0
            or self.rules.warnings
            or self.rules.dryrun
            or ignore_err
        ):
            raise subprocess.CalledProcessError(stat, step)

    @classmethod
    def bake_argfunc_step(cls, step: Partial) -> None:
        """Run a step made of a function and its arguments."""
        stat = step.run()
        if stat is not None:  # when something unexpected was returned
            print(stat)

    @classmethod
    def bake_func_step(cls, step: Callable[..., Any]) -> None:
        """Run a step made of a function without arguments."""
        stat = step()
        if stat is not None:  # when something unexpected was returned
            print(stat)

    def bake_bake_step(self, step: str) -> None:
        """Run a step that is another bake rule."""
        assert self.rules is not None
        if not self.rules.quiet:
            print(step)
        self.rules.make_tar(step[5:].strip())

    def bake(self) -> Tuple[int, str]:
        """Run recipe, check result or simply return if phony."""
        assert self.rules is not None

        if self.rules.dryrun:
            self.bake_dry_run()
            return 10, self.tar

        if self.rules.verbose:
            print(self)

        for step in self.recipe:
            if isinstance(step, str):
                if step.startswith('$bake'):
                    # invoke another rule (special string)
                    self.bake_bake_step(step)
                else:
                    # system call (string)
                    self.bake_string_step(step)

            elif isinstance(step, Partial):
                # function with arguments
                self.bake_argfunc_step(step)

            else:
                # simple function (no args)
                self.bake_func_step(step)

        # check if target is present unless its a phony rule
        if not self.phony:
            if not os.path.exists(self.tar):
                return 2, self.tar
            return 1, self.tar
        return -1, self.tar

    def bake_dry_run(self) -> None:
        """Dryrun recipe."""
        for i, step in enumerate(self.recipe):
            if isinstance(step, str):
                content = step
            elif isinstance(step, Partial):
                # content = step.func.__name__
                content = repr(step)
            else:
                content = step.__name__

            self.print_step(content, i+1)

    def __str__(self) -> str:
        """Human readable representation of a Rule instance."""
        return (
            f'target: {self.tar}\n'
            f'dependencies: {", ".join(self.deps)}\n'
            f'steps in recipe: {len(self.recipe)}\n'
        )

    def print_step(self, content: str, step: int) -> None:
        """Print content of a recipe step (for dry run + verbose)."""
        assert self.rules is not None
        level = f'[{self.rules.level}]'
        print(f'{PREFIX}{level} {self.tar}[{step}]: {content}')

    def __repr__(self) -> str:
        """Return detailed string with objects information."""
        return (
            '<Rule_object: ('
            f'target={self.tar}, '
            f'deps=[{", ".join(self.deps)}], '
            f'order_only_deps=[{", ".join(self.order_only_deps)}], '
            f'number_steps={len(self.recipe)}, '
            f'target={self.phony})>'
        )


class Rules:
    """Collection of Rule."""

    ok_stats = (0, -1, 1, 10)

    def __init__(
        self,
        rules: RuleDictT,
        **kwargs: bool
    ):
        """
        Create instance of Rules object.

        Parameters
        ----------
        rules:
            dictionary with Rules {tar: Rule(...)}


        Keyword arguments
        -----------------
        dryun:
            if `True`, recipes will not be executed (default `False`)

        verbose:
            if `True`, increase verbosity during run (default `False`)

        quiet:
            if `True`, reduce verbosity during run (default `False`)

        warnings:
            if `True`, bakepy errors will be caught and only a warning
            issued instead of an error raised (default `True`)

        force:
            if `True`, dependency step is skipped and rules are executed
            without their dependencies as triggers (default `False`)
        """
        self.rules = rules
        self.dryrun = kwargs.get('dryrun', False)
        self.verbose = kwargs.get('verbose', False)
        self.quiet = kwargs.get('quiet', False)
        self.warnings = kwargs.get('warnings', False)
        self.force = kwargs.get('force', False)
        self.level = 0
        self._link_rules()

    def _link_rules(self) -> None:
        """Establish link between the rule objects and rules."""
        for key in self.rules:
            self.rules[key].rules = self

    def __str__(self) -> str:
        """Return string representation of rules."""
        string = "Targets and dependencies:\n ---------\n"
        string += "\n".join(str(i) for i in self.rules.values())
        return string + "\n"

    def available(self) -> str:
        """Return list of targets in available rules."""
        string = "\nAvailable targets:  "
        string += ",  ".join(str(i) for i in self.rules)
        return string + "\n"

    def get_correct_rule(
        self, rulename: str,
    ) -> Union[Tuple[int, str], Rule]:
        """Find the right rule (including pattern rules)."""
        try:
            # an accurate match was found
            rule = self.rules[rulename]
        except KeyError:
            # check for pattern rule match
            if self.verbose:
                print(f'{PREFIX}there is no rule for {rulename}')
            pattern_rule = self.get_pattern_rule(rulename)

            if pattern_rule is None:
                if os.path.exists(rulename):
                    stat = 0, rulename
                else:
                    stat = -2, rulename
                return stat
            rule = pattern_rule
        else:
            # if recipe is empty, check for pattern rule match
            if rule.recipe == []:
                if self.verbose:
                    print(f'{PREFIX}the rule has no recipe')
                pattern_rule = self.get_pattern_rule(rulename)
                if pattern_rule is not None:
                    rule.adapt_recipe(pattern_rule)
        return rule

    def get_pattern_rule(
            self,
            rulename: str,
    ) -> Union[Rule, None]:
        """Return the pattern matched rule if possible."""
        try:
            specific_deps: List[str] = self.rules[rulename].deps
            specific_order_only_deps: List[str] = (
                self.rules[rulename].order_only_deps
            )
        except KeyError:
            specific_deps = []
            specific_order_only_deps = []

        for rule in self.rules.values():
            # consider only pattern rules
            if "%" in rule.tar:
                tar = rule.tar
                pattern = re.compile(
                    tar.replace('%', r'([a-z0-9\/\_\-\+]+)'))
                match = re.match(pattern, rulename)
                if match:
                    if self.verbose:
                        print(f'{PREFIX}accepted {tar}')
                    new_rule = Rule(
                        rulename,
                        remove_duplicates([
                            i.replace('%', match.group(1))
                            for i in rule.deps
                        ] + specific_deps),
                        remove_duplicates([
                            i.replace('%', match.group(1))
                            for i in rule.order_only_deps
                        ] + specific_order_only_deps),
                        rule.recipe,
                        phony=rule.phony,
                    )
                    new_rule.rules = rule.rules
                    return new_rule
        return None

    def check_deps(self, rule: Rule) -> List[int]:
        """Go through dependencies."""
        dep_stats: List[int] = []
        for dep in rule.deps:
            self.make_tar(dep)

        for oo_dep in rule.order_only_deps:
            self.make_tar(oo_dep)

        return dep_stats

    def make_tar_inner(
        self, rulename: str,
    ) -> Tuple[int, str]:
        """Make a target based on the rule."""
        rule = self.get_correct_rule(rulename)

        if not isinstance(rule, Rule):
            # rule contains a status instead
            return rule

        rule.parse_automatic_variables()

        self.check_deps(rule)

        if rule.phony or self.force:
            return rule.bake()

        if os.path.exists(rule.tar):
            target_date = os.stat(rule.tar).st_mtime_ns
        else:
            return rule.bake()

        for dep in rule.deps:
            dep_date = os.stat(dep).st_mtime_ns
            if dep_date > target_date:
                break
        else:
            return 0, rulename

        return rule.bake()

    def make_tar(self, rulename: str) -> Tuple[int, str]:
        """Wrap make_tar_inner and handle status/errors."""
        self._level_up()
        if self.verbose:
            self.print_status(6, rulename)
        status = self.make_tar_inner(rulename)
        if not self.quiet:
            self.print_status(*status)
        if (
            status[0] not in self.ok_stats
            and not self.warnings
            and not self.dryrun
        ):
            exception_handler(*status)
        self._level_down()
        return status

    def _level_up(self) -> None:
        """Increase level depth in rules baking."""
        self.level += 1

    def _level_down(self) -> None:
        """Decrease level depth in rules baking."""
        self.level -= 1

    def print_status(
        self,
        istat: int,
        tar: str
    ) -> None:
        """Report status."""
        level = f'[{self.level}] '

        if istat == -2:
            msg = (f'{level}ERROR: There is no rule for target "{tar}".')
        elif istat == -1:
            msg = (f'{level}{tar}: Performed (phony).')
        elif istat == 0:
            msg = (f'{level}{tar}: Already up to date.')
        elif istat == 1:
            msg = (f'{level}{tar}: Successfully updated.')
        elif istat == 6:
            if self.dryrun:
                msg = (f'{level}{tar}: Starting (dry run)...')
            else:
                msg = (f'{level}{tar}: Starting ...')
        elif istat == 10:
            msg = (f'{level}{tar}: done dry run.')
        else:
            msg = (f'{level}ERROR: Unable to make target "{tar}".')

        print(f'{PREFIX}{msg}')

    def set_flags(
        self,
        *,
        dry: Optional[bool] = None,
        warnings: Optional[bool] = None,
        quiet: Optional[bool] = None,
        verbose: Optional[bool] = None,
        force: Optional[bool] = None,
    ) -> Rules:
        """
        Update boolean flags of a Rule object.

        Either a single keyword value pair or multiple can be passed
        to this function.

        Available keywords:
            dry, warnings, quiet, verbose, force

        """
        if dry is not None:
            self.dryrun = dry
        if warnings is not None:
            self.warnings = warnings
        if quiet is not None:
            self.quiet = quiet
        if verbose is not None and quiet is not True:
            self.verbose = verbose
        if force is not None:
            self.force = force
        return self
